#!/bin/ksh

# Intellectual property information START
# 
# Copyright (c) 2021 Ivan Bityutskiy 
# 
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 
# Intellectual property information END

# Description START
#
# The script does temperature conversion in real
# time, with instant reaction to user's input.
# The script is written for OpenBSD's pdksh.
#
# Description END

# Shell settings START
set -o noglob
# Shell settings END

# Define functions START
function checkMin
{
  # Getting the unit of temperature
  local checkMode=$1
  # Fixing bc's output
  local checkValue="$(fixTemp "$2")"
  # Getting the fractional part of the number
  local checkValueZ="${checkValue#*.}"
  case $checkMode in
    1)
      # 1 is Celsius
      (( (${checkValue%.*} == -273) && (${checkValueZ#0} >= 15) || (${checkValue%.*} <= -274) )) && checkValue="-273.15"
      print -- "$checkValue"
      ;;
    2)
      # 2 is Fahrenheit
      (( (${checkValue%.*} == -459) && (${checkValueZ#0} >= 67) || (${checkValue%.*} <= -460) )) && checkValue="-459.67"
      print -- "$checkValue"
      ;;
    3)
      # 3 is Kelvin
      [[ "$checkValue" == '-'* ]] && checkValue=" 0.00"
      print -- "$checkValue"
      ;;
  esac
}

function makeZero
{
  # Function to add zeroes at the end of the value
  integer fracValue="$1"
  (( fracValue < 0 )) && fracValue=0
  integer zeroCounter=0
  local zeroChars=''
  until (( zeroCounter++ == fracValue ))
  do
    zeroChars="${zeroChars}0"
  done
  print -- "$zeroChars"
}

function fixTemp
{
  local testTemp="$1"
  # Dealing with bc's thing of not printing 0 in 0.5
  [[ "$testTemp" == '.'* ]] && testTemp=" 0$testTemp"
  [[ "$testTemp" == '-.'* ]] && testTemp="-0${testTemp#-}"
  # Dealing with bc returning 0 instead of 0.00
  [[ "$testTemp" == ? ]] && testTemp=" ${testTemp}.$(makeZero $bcScale)"
  # To make all values the same length,
  # dealing with bc not adding zeroes to the right
  local testTempZ="${testTemp#*.}"
  testTemp="${testTemp}$(makeZero $(( bcScale - ${#testTempZ} )))"
  print -- "$testTemp"
}

function printAlign
{
  # Function to align values by decimal separator.

  # Mode of the output (Celsius or Fahrenheit or Kelvin)
  local paTmode=$1
  # Getting temperature in Celsius
  local paCtemp="$2"
  [[ "$paCtemp" != '-'* ]] && paCtemp=" $paCtemp"
  # Getting the integer part of the value
  local paCtempInt="${paCtemp%.*}"
  # Getting temperature in Fahrenheit
  local paFtemp="$3"
  [[ "$paFtemp" != '-'* ]] && paFtemp=" $paFtemp"
  # Getting the integer part of the value
  local paFtempInt="${paFtemp%.*}"
  # Getting temperature in Kelvin
  local paKtemp="$4"
  [[ "$paKtemp" != '-'* ]] && paKtemp=" $paKtemp"
  # Getting the integer part of the value
  local paKtempInt="${paKtemp%.*}"

  # Checking againt Celsius, adding spaces
  while (( ${#paCtempInt} > ${#paFtempInt} ))
  do
    paFtemp=" ${paFtemp}"
    paFtempInt="${paFtemp%.*}"
  done
  while (( ${#paCtempInt} > ${#paKtempInt} ))
  do
    paKtemp=" ${paKtemp}"
    paKtempInt="${paKtemp%.*}"
  done

  # Checking againt Fahrenheit, adding spaces
  while (( ${#paFtempInt} > ${#paCtempInt} ))
  do
    paCtemp=" ${paCtemp}"
    paCtempInt="${paCtemp%.*}"
  done
  while (( ${#paFtempInt} > ${#paKtempInt} ))
  do
    paKtemp=" ${paKtemp}"
    paKtempInt="${paKtemp%.*}"
  done

  # Checking againt Kelvin, adding spaces
  while (( ${#paKtempInt} > ${#paCtempInt} ))
  do
    paCtemp=" ${paCtemp}"
    paCtempInt="${paCtemp%.*}"
  done
  while (( ${#paKtempInt} > ${#paFtempInt} ))
  do
    paFtemp=" ${paFtemp}"
    paFtempInt="${paFtemp%.*}"
  done

  # Printing aligned temperatures
  case $paTmode in
    1)
      # Celsius
      # Using \033(0 to use an alternate character set,
      # where f is displayed as a degree symbol
      # Using \033(B to go back to original character set
      print -- "\tCelsius:\t${paCtemp}\033(0f\033(B"
      print -- "\tFahrenheit:\t${paFtemp}\033(0f\033(B"
      print -- "\tKelvin:\t\t${paKtemp} K"
      ;;
    2)
      # Fahrenheit
      print -- "\tFahrenheit:\t${paFtemp}\033(0f\033(B"
      print -- "\tCelsius:\t${paCtemp}\033(0f\033(B"
      print -- "\tKelvin:\t\t${paKtemp} K"
      ;;
    3)
      # Kelvin
      print -- "\tKelvin:\t\t${paKtemp} K"
      print -- "\tCelsius:\t${paCtemp}\033(0f\033(B"
      print -- "\tFahrenheit:\t${paFtemp}\033(0f\033(B"
      ;;
  esac
}

function printHelp
{
  # Clearing the screen with CSI because use of
  # 'clear' program is slowing down the script
  print -n -- '\033[1;1H\033[2J'
  # Printing hotkey information
  print -u2 -- 'Hotkeys:'
  print -u2 -- ' r -0.01  degree\t\t\033[1m1\033[0m Celsius mode'
  print -u2 -- ' a -0.1   degree\t\t\033[1m2\033[0m Fahrenheit mode'
  print -u2 -- ' s -0.5   degrees\t\t\033[1m3\033[0m Kelvin mode'
  print -u2 -- ' d -1.0   degree'
  print -u2 -- ' f -5.0   degrees\t\t\033[1mz\033[0m reset to 0'
  print -u2 -- ' g -10.0  degrees\t\t> Increase fractional part'
  print -u2 -- ' h +10.0  degrees\t\t< Decrease fractional part'
  print -u2 -- ' j +5.0   degrees'
  print -u2 -- ' k +1.0   degree'
  print -u2 -- ' l +0.5   degrees'
  print -u2 -- ' ; +0.1   degree'
  print -u2 -- ' u +0.01  degree'
  print -u2 -- '\n \033[1mq\033[0m quit\n'
}

function processUserInput
{
  # Processing user's input one letter at a time
  userInput="$(dd bs=1 count=1 2> /dev/null)"
  # Calculating and displaying changes in temperature
  # according to user's input
  integer tempUnit=$1
  local tempValue="$2"
  local bcReply=''
  case $userInput in
    1)
      # Celsius
      print -- "C$tempValue"
      ;;
    2)
      # Fahrenheit
      print -- "F$tempValue"
      ;;
    3)
      # Kelvin
      print -- "K$tempValue"
      ;;
    r|R)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "($tempValue - 0.01)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    a|A)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "($tempValue - 0.1)" ; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    s|S)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "($tempValue - 0.5)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    d|D)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "($tempValue - 1)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    f|F)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "($tempValue - 5)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    g|G)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "($tempValue - 10)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    h|H)
      print -- "$(print -u8 -- "($tempValue + 10)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    j|J)
      print -- "$(print -u8 -- "($tempValue + 5)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    k|K)
      print -- "$(print -u8 -- "($tempValue + 1)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    l|L)
      print -- "$(print -u8 -- "($tempValue + 0.5)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    \;|\:)
      print -- "$(print -u8 -- "($tempValue + 0.1)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    u|U)
      print -- "$(print -u8 -- "($tempValue + 0.01)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    z|Z)
      print -- " 0.$(makeZero $bcScale)"
      ;;
    \.|\>)
      # Increase scale
      print -- "I$tempValue"
      ;;
    \,|\<)
      # Decrease scale
      print -- "D$tempValue"
      ;;
    q|Q)
      print -- 'Q'
      ;;
    *)
      print -- "$tempValue"
      ;;
  esac
}
# Define functions END

# Co-processes START
# Starting bc as a co-process
bc |&
# Making bc reading input from fd7,
# sending output to fd8
exec 7<&p
exec 8>&p
# Co-processes END

# Declare variables START
# Set the length of fractional part of the value
integer bcScale=2
# Declare varialble to hold temperature values
typeset cTemp=" 0.$(makeZero $bcScale)" fTemp=" 0.$(makeZero $bcScale)" kTemp=" 0.$(makeZero $bcScale)"
# tempMode's value can be changed to set default unit of
# temperature: 1 - Celsius; 2 - Fahrenheit; 3 - Kelvin
integer tempMode=1
# Changes to bcReply in subshells won't affect bcReply
# in parrent shell. Declaring it to make code more clear.
typeset bcReply=''
# Saving terminal emulator's settings
backup="$(stty -g)"
# Declare variables END

# BEGINNING OF SCRIPT
# Hiding the cursor
print -n -- '\033[?25l'
# Disabling canonical input
stty -icanon

# Reading from co-process doesn't work in this mode
while true
do
  # Setting the size of fractional part of the value
  print -u8 -- "scale=$bcScale"
  case $tempMode in
    1)
      # Calling a function to pretty print the temperature
      cTemp="$(fixTemp "$cTemp")"

      fTemp="$(print -u8 -- "$cTemp * 9 / 5 + 32"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      fTemp="$(fixTemp "$fTemp")"

      kTemp="$(print -u8 -- "$cTemp + 273.15"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      kTemp="$(fixTemp "$kTemp")"

      # Printing Hotkey information and clearing the screen
      printHelp

      # Printing aligned results
      printAlign $tempMode "$cTemp" "$fTemp" "$kTemp"

      # Calling a function to process user input
      cTemp="$(processUserInput 1 "$cTemp")"
      # Quitting if function returned 'Q'
      [[ "$cTemp" == 'Q' ]] && break
      # Changing temperature mode if function returned 'C', 'F', 'K'
      [[ "$cTemp" == 'C'* ]] && { cTemp="${cTemp#C}"; tempMode=1; }
      [[ "$cTemp" == 'F'* ]] && { cTemp="${cTemp#F}"; tempMode=2; }
      [[ "$cTemp" == 'K'* ]] && { cTemp="${cTemp#K}"; tempMode=3; }
      # Changing the size of fractional part of the value if function returned 'I', 'D'
      [[ "$cTemp" == 'I'* ]] && { cTemp="${cTemp#I}"; (( (bcScale < 12) && ++bcScale )); }
      [[ "$cTemp" == 'D'* ]] && { cTemp="${cTemp#D}"; (( (bcScale > 2) && --bcScale )); cTemp="${cTemp%%0}"; }
      ;;
    2)
      # Calling a function to pretty print the temperature
      fTemp="$(fixTemp "$fTemp")"

      cTemp="$(print -u8 -- "($fTemp - 32) * 5 / 9"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      cTemp="$(fixTemp "$cTemp")"

      kTemp="$(print -u8 -- "($fTemp + 459.67) * 5 / 9"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      kTemp="$(fixTemp "$kTemp")"

      # Printing Hotkey information and clearing the screen
      printHelp

      # Printing aligned results
      printAlign $tempMode "$cTemp" "$fTemp" "$kTemp"

      # Calling a function to process user input
      fTemp="$(processUserInput 2 "$fTemp")"
      # Quitting if function returned 'Q'
      [[ "$fTemp" == 'Q' ]] && break
      # Changing temperature mode if function returned 'C', 'F', 'K'
      [[ "$fTemp" == 'C'* ]] && { fTemp="${fTemp#C}"; tempMode=1; }
      [[ "$fTemp" == 'F'* ]] && { fTemp="${fTemp#F}"; tempMode=2; }
      [[ "$fTemp" == 'K'* ]] && { fTemp="${fTemp#K}"; tempMode=3; }
      # Changing the size of fractional part of the value if function returned 'I', 'D'
      [[ "$fTemp" == 'I'* ]] && { fTemp="${fTemp#I}"; (( (bcScale < 12) && ++bcScale )); }
      [[ "$fTemp" == 'D'* ]] && { fTemp="${fTemp#D}"; (( (bcScale > 2) && --bcScale )); fTemp="${fTemp%%0}"; }
      ;;
    3)
      # Calling a function to pretty print the temperature
      kTemp="$(fixTemp "$kTemp")"

      cTemp="$(print -u8 -- "$kTemp - 273.15"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      cTemp="$(fixTemp "$cTemp")"

      fTemp="$(print -u8 -- "$kTemp * 9 / 5 - 459.67"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      fTemp="$(fixTemp "$fTemp")"

      # Printing Hotkey information and clearing the screen
      printHelp

      # Printing aligned results
      printAlign $tempMode "$cTemp" "$fTemp" "$kTemp"

      # Calling a function to process user input
      kTemp="$(processUserInput 3 "$kTemp")"
      # Quitting if function returned 'Q'
      [[ "$kTemp" == 'Q' ]] && break
      # Changing temperature mode if function returned 'C', 'F', 'K'
      [[ "$kTemp" == 'C'* ]] && { kTemp="${kTemp#C}"; tempMode=1; }
      [[ "$kTemp" == 'F'* ]] && { kTemp="${kTemp#F}"; tempMode=2; }
      [[ "$kTemp" == 'K'* ]] && { kTemp="${kTemp#K}"; tempMode=3; }
      # Changing the size of fractional part of the value if function returned 'I', 'D'
      [[ "$kTemp" == 'I'* ]] && { kTemp="${kTemp#I}"; (( (bcScale < 12) && ++bcScale )); }
      [[ "$kTemp" == 'D'* ]] && { kTemp="${kTemp#D}"; (( (bcScale > 2) && --bcScale )); kTemp="${kTemp%%0}"; }
      ;;
  esac
done

# Restoring terminal emulator's settings
stty "$backup"
# Restoring the cursor and printing new line
# to fix the command prompt
print -- '\033[?25h'

# Co-processes START
# Closing the co-process
exec 7<&-
exec 8>&-
# Co-processes END

# Shell settings START
set +o noglob
# Shell settings END

# Exiting script with success status 0
exit 0

# END OF SCRIPT

