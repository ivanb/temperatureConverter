#!/bin/ksh

# Intellectual property information START
# 
# Copyright (c) 2021 Ivan Bityutskiy 
# 
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 
# Intellectual property information END

# Description START
#
# The script does temperature conversion in real
# time, with instant reaction to user's input.
# The script is written for OpenBSD's pdksh.
#
# Description END

# Shell settings START
set -o noglob
# Shell settings END

# Declare variables START
typeset ctemp=0 ftemp=0
# Saving terminal emulator's settings
backup="$(stty -g)"
# Declare variables END

# BEGINNING OF SCRIPT
# Disabling canonical input
stty -icanon


while true
do
  # Reading from co-process doesn't work in this mode
  # Dealing with bc's thing of not printing 0 in 0.5
  [[ "$ftemp" == '.'* ]] && ftemp="0$ftemp"
  [[ "$ftemp" == '-.'* ]] && ftemp="-0${ftemp#-}"
  # Need better way to test -459.67 number
  (( ${ftemp%.*} <= -459 )) && ftemp="-459.67"
  ctemp="$(print -- "scale=2; ($ftemp - 32) / 1.8" | bc)"
  # Dealing with bc's thing of not printing 0 in 0.5
  [[ "$ctemp" == '.'* ]] && ctemp="0$ctemp"
  [[ "$ctemp" == '-.'* ]] && ctemp="-0${ctemp#-}"
  # Clearing the screen
  clear
  # Printing hotkey information
  print -u2 -- 'Hotkeys:'
  print -u2 -- ' r -0.01  degree'
  print -u2 -- ' a -0.1   degree'
  print -u2 -- ' s -0.5   degrees'
  print -u2 -- ' d -1.0   degree'
  print -u2 -- ' f -5.0   degrees'
  print -u2 -- ' g -10.0  degrees'
  print -u2 -- ' h +10.0  degrees'
  print -u2 -- ' j +5.0   degrees'
  print -u2 -- ' k +1.0   degree'
  print -u2 -- ' l +0.5   degrees'
  print -u2 -- ' ; +0.1   degree'
  print -u2 -- ' u +0.01  degree'
  print -u2 -- '\nq: quit\n'
  # Using \033(0 to use an alternate character set,
  # where f shows as a degree symbol
  # Using \033(B to go back to original character set
  print -- "\tFahrenheit:\t${ftemp}\033(0f\033(B"
  print -- "\tCelsius:\t\033[31m${ctemp}\033(0f\033(B\033[0m"

  # Processing user's input one letter at a time
  userInput="$(dd bs=1 count=1 2> /dev/null)"
  # Calculating and displaying changes in temperature
  # according to user's input
  case $userInput in
    r|R)
      (( ${ftemp%.*} <= -459 )) && ftemp="-459.67"
      ftemp="$(print -- "scale=2; ($ftemp - 0.01)"  | bc)"
      ;;
    a|A)
      (( ${ftemp%.*} <= -459 )) && ftemp="-459.67"
      ftemp="$(print -- "scale=2; ($ftemp - 0.1)"  | bc)"
      ;;
    s|S)
      (( ${ftemp%.*} <= -459 )) && ftemp="-459.67"
      ftemp="$(print -- "scale=2; ($ftemp - 0.5)" | bc)"
      ;;
    d|D)
      (( ${ftemp%.*} <= -459 )) && ftemp="-459.67"
      ftemp="$(print -- "scale=2; ($ftemp - 1)" | bc)"
      ;;
    f|F)
      (( ${ftemp%.*} <= -459 )) && ftemp="-459.67"
      ftemp="$(print -- "scale=2; ($ftemp - 5)" | bc)"
      ;;
    g|G)
      (( ${ftemp%.*} <= -459 )) && ftemp="-459.67"
      ftemp="$(print -- "scale=2; ($ftemp - 10)" | bc)"
      ;;
    h|H)
      ftemp="$(print -- "scale=2; ($ftemp + 10)" | bc)"
      ;;
    j|J)
      ftemp="$(print -- "scale=2; ($ftemp + 5)" | bc)"
      ;;
    k|K)
      ftemp="$(print -- "scale=2; ($ftemp + 1)" | bc)"
      ;;
    l|L)
      ftemp="$(print -- "scale=2; ($ftemp + 0.5)" | bc)"
      ;;
    \;|\:)
      ftemp="$(print -- "scale=2; ($ftemp + 0.1)" | bc)"
      ;;
    u|U)
      ftemp="$(print -- "scale=2; ($ftemp + 0.01)" | bc)"
      ;;
    q|Q)
      break
      ;;
  esac
done

# Restoring terminal emulator's settings
stty "$backup"
# Printing new line to fix the prompt
print -- ''

# Shell settings START
set +o noglob
# Shell settings END

# Exiting script with success status - 0
exit 0

# END OF SCRIPT

