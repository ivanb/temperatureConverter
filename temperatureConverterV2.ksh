#!/bin/ksh

# Intellectual property information START
# 
# Copyright (c) 2021 Ivan Bityutskiy 
# 
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 
# Intellectual property information END

# Description START
#
# The script does temperature conversion in real
# time, with instant reaction to user's input.
# The script is written for OpenBSD's pdksh.
#
# Description END

# Shell settings START
set -o noglob
# Shell settings END

# Define functions START
function checkMin
{
  # Getting the unit of temperature
  local checkMode=$1
  # Fixing bc's output
  local checkValue="$(fixTemp "$2")"
  # Getting the fractional part of the number
  local checkValueZ="${checkValue#*.}"
  case $checkMode in
    1)
      # 1 is Celsius
      (( (${checkValue%.*} == -273) && (${checkValueZ#0} >= 15) || (${checkValue%.*} <= -274) )) && checkValue="-273.15"
      print -- "$checkValue"
      ;;
    2)
      # 2 is Fahrenheit
      (( (${checkValue%.*} == -459) && (${checkValueZ#0} >= 67) || (${checkValue%.*} <= -460) )) && checkValue="-459.67"
      print -- "$checkValue"
      ;;
    3)
      # 3 is Kelvin
      [[ "$checkValue" == '-'* ]] && checkValue=" 0.00"
      print -- "$checkValue"
      ;;
  esac
}

function fixTemp
{
  local testTemp="$1"
  # Dealing with bc's thing of not printing 0 in 0.5
  [[ "$testTemp" == '.'* ]] && testTemp=" 0$testTemp"
  [[ "$testTemp" == '-.'* ]] && testTemp="-0${testTemp#-}"
  # Dealing with bc returning 0 instead of 0.00
  [ "$testTemp" == '0' ] && testTemp=" 0.00"
  # Aligning positive and negative temperature values
  [[ "$testTemp" == '-'* ]] || [[ "$testTemp" == ' '* ]] || testTemp=" ${testTemp}"
  print -- "$testTemp"
}

function printHelp
{
  # Clearing the screen with CSI because use of
  # 'clear' program is slowing down the script
  print -n -- '\033[1;1H\033[2J'
  # Printing hotkey information
  print -u2 -- 'Hotkeys:'
  print -u2 -- ' r -0.01  degree\t\t1 Celsius mode'
  print -u2 -- ' a -0.1   degree\t\t2 Fahrenheit mode'
  print -u2 -- ' s -0.5   degrees\t\t3 Kelvin mode'
  print -u2 -- ' d -1.0   degree'
  print -u2 -- ' f -5.0   degrees'
  print -u2 -- ' g -10.0  degrees'
  print -u2 -- ' h +10.0  degrees'
  print -u2 -- ' j +5.0   degrees'
  print -u2 -- ' k +1.0   degree'
  print -u2 -- ' l +0.5   degrees'
  print -u2 -- ' ; +0.1   degree'
  print -u2 -- ' u +0.01  degree'
  print -u2 -- '\n \033[1mq: quit\033[0m\n'
}

function processUserInput
{
  # Processing user's input one letter at a time
  userInput="$(dd bs=1 count=1 2> /dev/null)"
  # Calculating and displaying changes in temperature
  # according to user's input
  integer tempUnit=$1
  local tempValue="$2"
  local bcReply=''
  case $userInput in
    1)
      # Celsius
      print -- "C$tempValue"
      ;;
    2)
      # Fahrenheit
      print -- "F$tempValue"
      ;;
    3)
      # Kelvin
      print -- "K$tempValue"
      ;;
    r|R)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "scale=2; ($tempValue - 0.01)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    a|A)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "scale=2; ($tempValue - 0.1)" ; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    s|S)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "scale=2; ($tempValue - 0.5)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    d|D)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "scale=2; ($tempValue - 1)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    f|F)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "scale=2; ($tempValue - 5)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    g|G)
      print -- "$(checkMin $tempUnit "$(print -u8 -- "scale=2; ($tempValue - 10)"; read -u7 -- bcReply; print -- "$bcReply")")"
      ;;
    h|H)
      print -- "$(print -u8 -- "scale=2; ($tempValue + 10)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    j|J)
      print -- "$(print -u8 -- "scale=2; ($tempValue + 5)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    k|K)
      print -- "$(print -u8 -- "scale=2; ($tempValue + 1)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    l|L)
      print -- "$(print -u8 -- "scale=2; ($tempValue + 0.5)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    \;|\:)
      print -- "$(print -u8 -- "scale=2; ($tempValue + 0.1)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    u|U)
      print -- "$(print -u8 -- "scale=2; ($tempValue + 0.01)"; read -u7 -- bcReply; print -- "$bcReply")"
      ;;
    q|Q)
      print -- 'Q'
      ;;
    *)
      print -- "$tempValue"
      ;;
  esac
}
# Define functions END

# Co-processes START
# Starting bc as a co-process
bc |&
# Making bc reading input from fd7,
# sending output to fd8
exec 7<&p
exec 8>&p
# Co-processes END

# Declare variables START
typeset cTemp=' 0.00' fTemp=' 0.00' kTemp=' 0.00'
# tempMode's value can be changed to set default unit of
# temperature: 1 - Celsius; 2 - Fahrenheit; 3 - Kelvin
integer tempMode=1
# Changes to bcReply in subshells won't affect bcReply
# in parrent shell. Declaring it to make code more clear.
typeset bcReply=''
# Saving terminal emulator's settings
backup="$(stty -g)"
# Declare variables END

# BEGINNING OF SCRIPT
# Hiding the cursor
print -n -- '\033[?25l'
# Disabling canonical input
stty -icanon

# Reading from co-process doesn't work in this mode
while true
do
  case $tempMode in
    1)
      # Calling a function to pretty print the temperature
      cTemp="$(fixTemp "$cTemp")"

      fTemp="$(print -u8 -- "scale=2; $cTemp * 9 / 5 + 32"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      fTemp="$(fixTemp "$fTemp")"

      kTemp="$(print -u8 -- "scale=2; $cTemp + 273.15"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      kTemp="$(fixTemp "$kTemp")"

      # Printing Hotkey information and clearing the screen
      printHelp

      # Using \033(0 to use an alternate character set,
      # where f is displayed as a degree symbol
      # Using \033(B to go back to original character set
      print -- "\tCelsius:\t\033[31m${cTemp}\033(0f\033(B\033[0m"
      print -- "\tFahrenheit:\t${fTemp}\033(0f\033(B"
      print -- "\tKelvin:\t\t\033[34m${kTemp} K\033[0m"

      # Calling a function to process user input
      cTemp="$(processUserInput 1 "$cTemp")"
      [[ "$cTemp" == 'Q' ]] && break
      [[ "$cTemp" == 'C'* ]] && { cTemp="${cTemp#C}"; tempMode=1; }
      [[ "$cTemp" == 'F'* ]] && { cTemp="${cTemp#F}"; tempMode=2; }
      [[ "$cTemp" == 'K'* ]] && { cTemp="${cTemp#K}"; tempMode=3; }
      ;;
    2)
      # Calling a function to pretty print the temperature
      fTemp="$(fixTemp "$fTemp")"

      cTemp="$(print -u8 -- "scale=2; ($fTemp - 32) * 5 / 9"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      cTemp="$(fixTemp "$cTemp")"

      kTemp="$(print -u8 -- "scale=2; ($fTemp + 459.67) * 5 / 9"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      kTemp="$(fixTemp "$kTemp")"

      # Printing Hotkey information and clearing the screen
      printHelp

      # Using \033(0 to use an alternate character set,
      # where f is displayed as a degree symbol
      # Using \033(B to go back to original character set
      print -- "\tFahrenheit:\t${fTemp}\033(0f\033(B"
      print -- "\tCelsius:\t\033[31m${cTemp}\033(0f\033(B\033[0m"
      print -- "\tKelvin:\t\t\033[34m${kTemp} K\033[0m"

      # Calling a function to process user input
      fTemp="$(processUserInput 2 "$fTemp")"
      [[ "$fTemp" == 'Q' ]] && break
      [[ "$fTemp" == 'C'* ]] && { fTemp="${fTemp#C}"; tempMode=1; }
      [[ "$fTemp" == 'F'* ]] && { fTemp="${fTemp#F}"; tempMode=2; }
      [[ "$fTemp" == 'K'* ]] && { fTemp="${fTemp#K}"; tempMode=3; }
      ;;
    3)
      # Calling a function to pretty print the temperature
      kTemp="$(fixTemp "$kTemp")"

      cTemp="$(print -u8 -- "scale=2; $kTemp - 273.15"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      cTemp="$(fixTemp "$cTemp")"

      fTemp="$(print -u8 -- "scale=2; $kTemp * 9 / 5 - 459.67"; read -u7 -- bcReply; print -- "$bcReply")"
      # Calling a function to pretty print the temperature
      fTemp="$(fixTemp "$fTemp")"

      # Printing Hotkey information and clearing the screen
      printHelp

      # Using \033(0 to use an alternate character set,
      # where f is displayed as a degree symbol
      # Using \033(B to go back to original character set
      print -- "\tKelvin:\t\t\033[34m${kTemp} K\033[0m"
      print -- "\tCelsius:\t\033[31m${cTemp}\033(0f\033(B\033[0m"
      print -- "\tFahrenheit:\t${fTemp}\033(0f\033(B"

      # Calling a function to process user input
      kTemp="$(processUserInput 3 "$kTemp")"
      [[ "$kTemp" == 'Q' ]] && break
      [[ "$kTemp" == 'C'* ]] && { kTemp="${kTemp#C}"; tempMode=1; }
      [[ "$kTemp" == 'F'* ]] && { kTemp="${kTemp#F}"; tempMode=2; }
      [[ "$kTemp" == 'K'* ]] && { kTemp="${kTemp#K}"; tempMode=3; }
      ;;
  esac
done

# Restoring terminal emulator's settings
stty "$backup"
# Restoring the cursor and printing new line
# to fix the command prompt
print -- '\033[?25h'

# Co-processes START
# Closing the co-process
exec 7<&-
exec 8>&-
# Co-processes END

# Shell settings START
set +o noglob
# Shell settings END

# Exiting script with success status 0
exit 0

# END OF SCRIPT

